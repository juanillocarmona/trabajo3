**TAREA 3:**
------------

**Fecha:** 12-Feb-14

**Autor:** Carmona L�pez, Juan Jes�s

**Visualizaci�n:**

* Abrir el documento "index.html"

* Desde el archivo anterior, podr� acceder al resto de la informaci�n del trabajo

**Realizaci�n:**

#. Us� el lenguaje *HTML* en la creac�on de las p�ginas que componen la tarea individualmente.

#. Cre� el documento "index.html" con los enlaces a las dem�s p�ginas.


**Objetivos:**

* Realic� la tarea bas�ndome en la idea de una web que ofreciera informaci�n puramente visual y atractiva sobre un hotel.

**Conclusiones / Valoraci�n personal:**

La tarea me demostr� un camino nuevo que desconoc�a, y aunque pedregoso, bastante interesante. 
Me sent� satisfecho por la tarea realizada, por ser el "creador" de mi primera web, con la �nica ayuda del libro y muchisimas ganas.